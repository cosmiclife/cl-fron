# этап сборки (build stage)
FROM node:10.15.0 as build-stage
WORKDIR /app
COPY app/package*.json ./
COPY env ./.env
RUN npm install
#RUN npm i -g @nestjs/cli
RUN npm i -g @nuxtjs/svg
COPY app/ .
RUN npm run build

FROM nginx as production-stage
RUN mkdir /app
COPY --from=build-stage /app/dist /app
COPY nginx.conf /etc/nginx/nginx.conf
