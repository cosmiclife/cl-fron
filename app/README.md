# CosmicLifeGame

Браузерная онлайн стратегия, кликер

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

## ENV (.env)

```js
API_ENDPOINT = "https://back.cosmiclife.ru/api/v1/"
TOKEN="52cc2d0407b124a85e758a9190faa24fa5f4c771"
PASSPORT_CLIENT_SECRET="2"
PASSPORT_PASSWORD_GRANT_ID="JkIcUKtKtaDHL8GbcqRFf6hIjsDaAS6ZWon0PPMa"
PASSPORT_PASSWORD_GRANT_SECRET="D4NDq1KxbNnPUl0jB5sgev9oKAVE9wjZ2127Kwc3ut1lQ1uwn3hLVWDkM16Ick3gdCuM7pNRrWBIUM79Ck9EE3ySKebujlvbC2CuAKapvPPKGFPCiWifu76t4uslFSug"
MOBILE_APP_ID = "a38ddadc-9fdd-476a-89ff-8e2cd448c9a9"
APP_LOCALE = "ru"
AUTH_EMAIL_VALIDATE = "on"
DEBUG = "on"
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
