export default {
  data() {
    return { loading: true }
  },
  methods: {
    getDataNamesFromAPI(query) {
      return new Promise((resolve, reject) => {
        const items = this.dataNamesFromDB().find((e) => e.Name === query)
        setTimeout(() => {
          this.loading = false
          resolve({
            items
          })
        }, 1000)
      })
    },
    getDataSureNamesFromAPI(query) {
      return new Promise((resolve, reject) => {
        const items = this.dataSurenamesFromDB().find((e) => e.Surname === query)
        setTimeout(() => {
          this.loading = false
          resolve({
            items
          })
        }, 1000)
      })
    },
    dataNamesFromDB() {
      return require('russian_names.json')
    },
    dataSurenamesFromDB() {
      return require('russian_surnames.json')
    }
  }
}
