import Vue from 'vue'

export default function($this) {
  Vue.prototype.requestPlayerNameAPI = (params) => {
    return requestAPI('avatars/generate-name/', 'GET', params)
  }

  Vue.prototype.requestPlayerListAPI = (params) => {
    return requestAPI('avatars/hero-avatar/', 'GET', params)
  }

  Vue.prototype.requestStartAPI = (params) => {
    return requestAPI('avatars/start/', 'POST', params)
  }

  Vue.prototype.requestSignInAPI = (params) => {
    return requestAPI('accounts/login/', 'POST', params)
  }

  Vue.prototype.requestActuailizeMeAPI = (id) => {
    return requestAPI('avatars/me/' + id + '/', 'GET', {})
  }

  Vue.prototype.requestVictoryAPI = (id, params) => {
    return requestAPI('avatars/victory/' + id + '/', 'POST', params)
  }

  Vue.prototype.requestCartAPI = (id, params) => {
    return requestAPI('avatars/cart/' + id + '/', 'POST', params)
  }

  function requestAPI(apiUser, method, params, headerUser) {
    const extraHeader = { Authorization: 'Token ' + process.env.TOKEN }
    const header = { ...headerUser, ...extraHeader }
    const api = `${process.env.API_ENDPOINT}${apiUser}`

    let request
    try {
      if (method === 'OPTIONS') {
        request = $this.$axios.$options(api, params, header)
      }
      if (method === 'PATCH') {
        request = $this.$axios.$patch(api, params, header)
      }
      if (method === 'POST') {
        request = $this.$axios.$post(api, params, {
          headers: { common: header }
        })
      }
      if (method === 'GET') {
        request = $this.$axios.$get(api, {
          params,
          headers: { common: header }
        })
      }
      if (method === 'PUT') {
        request = $this.$axios.$put(api, params, header)
      }
      if (method === 'DELETE') {
        request = $this.$axios.$delete(api, params, header)
      }

      request.then((res) => {
        process.env.DEBUG === 'on' &&
          Vue.prototype.$bus.$emit('console', {
            type: 'log',
            message: { text: 'API: ' + api + ' response', value: res }
          })
      })

      request.catch((err) => {
        process.env.DEBUG === 'on' &&
          Vue.prototype.$bus.$emit('console', {
            type: 'error',
            message: { text: 'ERROR ', value: err }
          })

        Vue.prototype.$bus.$emit('alert', {
          type: 'error',
          message: err.response.data.error.message
        })
      })

      return request
    } catch (e) {
      process.env.DEBUG === 'on' &&
        Vue.prototype.$bus.$emit('console', {
          type: 'error',
          message: { text: 'CRITICAL ERROR ', value: e }
        })
    }
  }
}
