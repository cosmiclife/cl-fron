import Vue from 'vue'

export default function({ $axios, $store }) {
  $axios.onRequest((config) => {
    if (
      typeof config.url !== 'undefined' &&
      typeof config.data !== 'undefined'
    ) {
      process.env.DEBUG === 'on' &&
        Vue.prototype.$bus.$emit('console', {
          type: 'log',
          message: {
            text: 'Making request to ' + config.url + ' send data ',
            res: config.data
          }
        })
    }
  })
  $axios.onError((error) => {
    let techMessage = ''
    let code = 0
    if (error.response) {
      code = parseInt(error.response && error.response.status)

      if (code === 401) {
        Vue.prototype.$bus.$emit('signal', { code: 'logout' })
      }

      if (code === 401) {
        techMessage = 'Требуется авторизация'
      } else if (code === 404) {
        if (error.response.data.detail) techMessage = 'Отсутствуют данные'
        else {
          techMessage = 'API не доступно по адресу: ' + error.config.url
        }
      } else if (code === 500) {
        techMessage =
          'Серверная ошибка API. ' +
          ' ' +
          error.response.data.substring(0, 130) +
          '...'
      } else {
        techMessage = code + ' ' + error.response.statusText
      }
    } else {
      techMessage = 'Ошибка подключения к API'
    }

    if (process.env.DEBUG === 'on' && techMessage) {
      /* eslint-disable no-console */
      console.log(code, techMessage)
      /* eslint-enable no-console */

      Vue.prototype.$bus.$emit('alert', {
        type: 'error',
        message: techMessage,
        timeout: 10000
      })
    }
  })
  $axios.onResponse((response) => {})
}
