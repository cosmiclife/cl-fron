import Vue from 'vue'
import VueNativeSock from 'vue-native-websocket'

Vue.use(VueNativeSock, process.env.REMOTE_WS, { format: 'json' })
