export const state = () => ({
  gameId: null,
  game: null,
  achieves: [],
  newAchieves: [],
  ranking: {},
  avatar: {}
})

export const mutations = {
  SET_GAME(state, data) {
    state.game = data
  },
  SET_ID(state, data) {
    state.gameId = data.avatar_id
  },
  SET_LOGOUT(state) {
    state.gameId = null
    state.game = null
  },
  SET_ACHIEVES(state, data) {
    state.achieves = data.achievemets
    state.newAchieves = data.new_achievemets
  },
  SET_RANKING(state, data) {
    state.ranking = data
  },
  SET_AVATAR(state, data) {
    state.avatar = data
  }
}
